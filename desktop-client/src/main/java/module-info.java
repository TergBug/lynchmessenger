module com.messenger.lynch.client {
    requires javafx.controls;
    requires javafx.fxml;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires org.apache.httpcomponents.httpclient;
    requires org.apache.httpcomponents.httpcore;
    requires java.validation;

    opens com.messenger.lynch.client to javafx.fxml;
    opens com.messenger.lynch.client.controller to javafx.fxml;
    exports com.messenger.lynch.client;
    exports com.messenger.lynch.client.controller;
    exports com.messenger.lynch.client.dto;
}