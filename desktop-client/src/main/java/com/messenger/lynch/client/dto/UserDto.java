package com.messenger.lynch.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserDto {

    private String nickname;

    private String name;

    private String avatarLink;

    public UserDto() {
    }

    public UserDto(String nickname, String name, String avatarLink) {
        this.nickname = nickname;
        this.name = name;
        this.avatarLink = avatarLink;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

}
