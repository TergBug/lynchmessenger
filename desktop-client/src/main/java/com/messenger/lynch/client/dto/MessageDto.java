package com.messenger.lynch.client.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageDto {

    private String id;

    private String userNickname;

    private String userName;

    private String userAvatar;

    private String chatId;

    private LocalDateTime time;

    private String text;

    public MessageDto() {
    }

    @JsonCreator
    public MessageDto(@JsonProperty("id") String id, @JsonProperty("userNickname") String userNickname,
                      @JsonProperty("userName") String userName, @JsonProperty("userAvatar") String userAvatar,
                      @JsonProperty("chatId") String chatId, @JsonProperty("time") String time,
                      @JsonProperty("text") String text) {
        this.id = id;
        this.userNickname = userNickname;
        this.userName = userName;
        this.userAvatar = userAvatar;
        this.chatId = chatId;
        this.time = LocalDateTime.parse(time);
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
