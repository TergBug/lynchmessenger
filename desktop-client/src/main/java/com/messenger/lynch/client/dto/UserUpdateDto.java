package com.messenger.lynch.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserUpdateDto {

    private String name;

    private String avatarLink;

    public UserUpdateDto() {
    }

    public UserUpdateDto(String name, String avatarLink) {
        this.name = name;
        this.avatarLink = avatarLink;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

}
