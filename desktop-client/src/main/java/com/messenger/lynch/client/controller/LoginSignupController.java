package com.messenger.lynch.client.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.messenger.lynch.client.Starter;
import com.messenger.lynch.client.constant.ApplicationContext;
import com.messenger.lynch.client.constant.Constants;
import com.messenger.lynch.client.controller.util.Method;
import com.messenger.lynch.client.controller.util.Requester;
import com.messenger.lynch.client.controller.util.Response;
import com.messenger.lynch.client.dto.UserCreateDto;
import com.messenger.lynch.client.dto.UserDto;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import org.apache.http.NameValuePair;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class LoginSignupController {

    @FXML
    private TextField username;

    @FXML
    private PasswordField password;

    @FXML
    private VBox form;

    @FXML
    public void onUpdateErrorMsg() {
        form.getChildren().removeIf(el -> el.getId() != null && el.getId().equals("errorMsg"));
    }

    @FXML
    public void onSwitchToSignupButton() {
        Starter.getGlobalScene().assignToRoot("signup");
    }

    @FXML
    public void onSwitchToLoginButton() {
        Starter.getGlobalScene().assignToRoot("login");
    }

    @FXML
    public void onSignupButton() {
        onUpdateErrorMsg();
        Requester requester = new Requester(new BasicCookieStore(), Method.POST, Constants.SIGNUP_URL);
        UserCreateDto userCreateDto = new UserCreateDto(username.getText(), username.getText(), password.getText(), "");

        Response response = requester.doRequestWithObject(userCreateDto);
        if (response.getCode() != 200) {
            response.getErrorMsg().setText(response.getCode() == 409
                    ? "User " + userCreateDto.getNickname() + " already exists"
                    : "Something wrong");
            form.getChildren().add(4, response.getErrorMsg());
            return;
        }

        Starter.getGlobalScene().assignToRoot("login");
    }

    @FXML
    public void onLoginButton() {
        onUpdateErrorMsg();
        try {
            BasicCookieStore cookieStore = new BasicCookieStore();
            ApplicationContext.addObject(cookieStore);
            Requester requester = new Requester(cookieStore, Method.POST, Constants.LOGIN_URL);

            List<NameValuePair> params = new ArrayList<>();
            params.add(new BasicNameValuePair("nickname", username.getText()));
            params.add(new BasicNameValuePair("password", password.getText()));

            Response response = requester.doRequestWithParams(params);
            if (response.getCode() != 200) {
                response.getErrorMsg().setText("Wrong username or password");
                form.getChildren().add(3, response.getErrorMsg());
                return;
            }

            UserDto userDto = new ObjectMapper().readValue(response.getJson(), UserDto.class);
            ApplicationContext.addObjectByName("currentUser", userDto);
            Starter.getGlobalScene().assignToRoot("chatList");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
