package com.messenger.lynch.client.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.messenger.lynch.client.Starter;
import com.messenger.lynch.client.constant.ApplicationContext;
import com.messenger.lynch.client.constant.Constants;
import com.messenger.lynch.client.controller.util.Method;
import com.messenger.lynch.client.controller.util.Requester;
import com.messenger.lynch.client.controller.util.Response;
import com.messenger.lynch.client.dto.ChatDto;
import com.messenger.lynch.client.dto.MessageCreateDto;
import com.messenger.lynch.client.dto.MessageDto;
import com.messenger.lynch.client.dto.UserDto;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;

import java.net.URL;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.ResourceBundle;

public class ChatController implements Initializable {

    @FXML
    private Circle userAvatar;

    @FXML
    private Circle currentUserAvatar;

    @FXML
    private Text username;

    @FXML
    private TextArea enteredMessage;

    @FXML
    private VBox messageList;

    @FXML
    private VBox commonList;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init(loadChat());
    }

    @FXML
    public void onGoToProfile() {
        ApplicationContext.removeObjectByName("currentChatId");
        Starter.getGlobalScene().assignToRoot("profile");
    }

    @FXML
    public void onGoToChatList() {
        ApplicationContext.removeObjectByName("currentChatId");
        Starter.getGlobalScene().assignToRoot("chatList");
    }

    @FXML
    public void onUpdateChat() {
        init(loadChat());
    }

    @FXML
    public void onSend() {
        if (enteredMessage.getText().isBlank()) {
            return;
        }
        try {
            Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                    Method.POST, Constants.ADD_MESSAGE_URL);

            MessageCreateDto messageCreateDto = new MessageCreateDto(
                    (String) ApplicationContext.getObjectByName("currentChatId"),
                    Constants.TIMEZONE,
                    enteredMessage.getText());
            Response response = requester.doRequestWithObject(messageCreateDto);

            if (response.getCode() != 200) {
                response.getErrorMsg().setText("Cannot add message to chat "
                        + ApplicationContext.getObjectByName("currentChatId"));
                commonList.getChildren().add(1, response.getErrorMsg());
                return;
            }

            init(new ObjectMapper().readValue(response.getJson(), ChatDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    private void init(ChatDto inputChat) {
        messageList.getChildren().clear();
        enteredMessage.setText("");
        UserDto currentUser = (UserDto) ApplicationContext.getObjectByName("currentUser");

        userAvatar.setFill(new ImagePattern(new Image(inputChat.getUserAvatar())));
        username.setText(inputChat.getUserNickname().equals(currentUser.getNickname()) ? "My notes" : inputChat.getUserName());
        currentUserAvatar.setFill(new ImagePattern(new Image(currentUser.getAvatarLink())));

        inputChat.getMessages().stream().sorted(Comparator.comparing(MessageDto::getTime)).forEach(el -> {
            BorderPane messageLine = (BorderPane) Starter.getGlobalScene().loadView("message");
            if (!(messageLine.getLeft() instanceof Circle)
                    || !(messageLine.getCenter() instanceof VBox)
                    || !(messageLine.getRight() instanceof Text)) {
                return;
            }
            messageLine.getProperties().put("messageId", el.getId());

            ((Circle) messageLine.getLeft()).setFill(new ImagePattern(new Image(el.getUserAvatar() == null
                    ? Constants.DEFAULT_AVATAR
                    : el.getUserAvatar())));

            String authorName = el.getUserNickname().equals(currentUser.getNickname())
                    ? el.getUserName() + " (you)"
                    : el.getUserName();
            ((VBox) messageLine.getCenter()).getChildren().stream()
                    .filter(tx -> tx.getId().equals("authorName") && tx instanceof Text)
                    .forEach(tx -> ((Text) tx).setText(authorName));
            ((VBox) messageLine.getCenter()).getChildren().stream()
                    .filter(tx -> tx.getId().equals("messageText") && tx instanceof Text)
                    .forEach(tx -> ((Text) tx).setText(el.getText()));

            String time = el.getTime().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss"));
            ((Text) messageLine.getRight()).setText(time);
            messageList.getChildren().add(messageLine);
        });
    }

    private ChatDto loadChat() {
        try {
            Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                    Method.GET, Constants.FETCH_MESSAGES_URL
                    + ApplicationContext.getObjectByName("currentChatId"));

            Response response = requester.doRequest();
            if (response.getCode() != 200) {
                response.getErrorMsg().setText("Cannot load chat "
                        + ApplicationContext.getObjectByName("currentChatId"));
                commonList.getChildren().add(1, response.getErrorMsg());
                return new ChatDto();
            }

            return new ObjectMapper().readValue(response.getJson(), ChatDto.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
