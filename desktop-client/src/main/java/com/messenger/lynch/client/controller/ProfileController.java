package com.messenger.lynch.client.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.messenger.lynch.client.Starter;
import com.messenger.lynch.client.constant.ApplicationContext;
import com.messenger.lynch.client.constant.Constants;
import com.messenger.lynch.client.controller.util.Method;
import com.messenger.lynch.client.controller.util.Requester;
import com.messenger.lynch.client.controller.util.Response;
import com.messenger.lynch.client.dto.UserDto;
import com.messenger.lynch.client.dto.UserUpdateDto;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;

import java.net.URL;
import java.util.ResourceBundle;

public class ProfileController implements Initializable {

    @FXML
    private Text nickname;

    @FXML
    private TextField name;

    @FXML
    private TextField avatarUrl;

    @FXML
    private ImageView avatar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }

    private void init() {
        UserDto userDto = (UserDto) ApplicationContext.getObjectByName("currentUser");
        nickname.setText(userDto.getNickname());
        name.setText(userDto.getName());
        avatarUrl.setText(userDto.getAvatarLink());
        Image avatarImg = new Image(userDto.getAvatarLink());
        avatar.setImage(avatarImg);
    }

    @FXML
    public void onGoToChatList() {
        Starter.getGlobalScene().assignToRoot("chatList");
    }

    @FXML
    public void onLogout() {
        Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                Method.DELETE, Constants.LOGOUT_URL);

        Response response = requester.doRequest();
        if (response.getCode() != 204) {
            response.getErrorMsg().setText("Something wrong");
            ((HBox) nickname.getParent()).getChildren().add(response.getErrorMsg());
            return;
        }

        ApplicationContext.removeObjectByName("currentUser");
        Starter.getGlobalScene().assignToRoot("login");
    }

    @FXML
    public void onDeleteAvatar() {
        avatarUrl.setText("");
        name.setText(((UserDto) ApplicationContext.getObjectByName("currentUser")).getName());
        onUpdateProfile();
    }

    @FXML
    public void onUpdateProfile() {
        try {
            Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                    Method.PUT, Constants.UPDATE_PROFILE_URL);

            Response response = requester.doRequestWithObject(new UserUpdateDto(name.getText(), avatarUrl.getText().strip()));
            if (response.getCode() != 200) {
                response.getErrorMsg().setText("Cannot update profile");
                ((HBox) nickname.getParent()).getChildren().add(response.getErrorMsg());
                return;
            }

            UserDto userDto = new ObjectMapper().readValue(response.getJson(), UserDto.class);
            ApplicationContext.addObjectByName("currentUser", userDto);
            Starter.getGlobalScene().assignToRoot("profile");
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
