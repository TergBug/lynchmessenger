package com.messenger.lynch.client.controller.util;

import javafx.scene.text.Text;

public class Response {

    private int code;
    private Text errorMsg;
    private String json;

    public Response(int code, Text errorMsg, String json) {
        this.code = code;
        this.errorMsg = errorMsg;
        this.json = json;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Text getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(Text errorMsg) {
        this.errorMsg = errorMsg;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

}
