package com.messenger.lynch.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageCreateDto {

    private String chatId;

    private String timeZone;

    private String text;

    public MessageCreateDto() {
    }

    public MessageCreateDto(String chatId, String timeZone, String text) {
        this.chatId = chatId;
        this.timeZone = timeZone;
        this.text = text;
    }

    public String getChatId() {
        return chatId;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
