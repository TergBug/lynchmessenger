package com.messenger.lynch.client.constant;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {

    private static final Map<String, Object> OBJECT_MAP = new HashMap<>();

    public static Object getObject(Class objClass) {
        return OBJECT_MAP.get(objClass.getName());
    }

    public static Object getObjectByName(String objName) {
        return OBJECT_MAP.get(objName);
    }

    public static void addObject(Object obj) {
        OBJECT_MAP.put(obj.getClass().getName(), obj);
    }

    public static void removeObject(Object obj) {
        OBJECT_MAP.remove(obj.getClass().getName());
    }

    public static void addObjectByName(String name, Object obj) {
        OBJECT_MAP.put(name, obj);
    }

    public static void removeObjectByName(String name) {
        OBJECT_MAP.remove(name);
    }

}
