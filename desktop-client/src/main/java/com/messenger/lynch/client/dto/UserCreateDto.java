package com.messenger.lynch.client.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotBlank;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserCreateDto {

    @NotBlank(message = "Nickname must not be a blank!")
    private String nickname;

    @NotBlank(message = "Name must not be a blank!")
    private String name;

    @NotBlank(message = "Password must not be a blank!")
    private String password;

    private String avatarLink;

    public UserCreateDto() {
    }

    public UserCreateDto(String nickname, String name, String password, String avatarLink) {
        this.nickname = nickname;
        this.name = name;
        this.password = password;
        this.avatarLink = avatarLink;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

}
