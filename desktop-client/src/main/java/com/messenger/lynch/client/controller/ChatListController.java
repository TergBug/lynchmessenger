package com.messenger.lynch.client.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.messenger.lynch.client.Starter;
import com.messenger.lynch.client.constant.ApplicationContext;
import com.messenger.lynch.client.constant.Constants;
import com.messenger.lynch.client.controller.util.Method;
import com.messenger.lynch.client.controller.util.Requester;
import com.messenger.lynch.client.controller.util.Response;
import com.messenger.lynch.client.dto.ChatDto;
import com.messenger.lynch.client.dto.MessageDto;
import com.messenger.lynch.client.dto.UserDto;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.ImagePattern;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;

import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

public class ChatListController implements Initializable {

    @FXML
    private VBox list;

    @FXML
    private TextField findUser;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }

    @FXML
    public void onUpdateList() {
        init();
    }

    @FXML
    public void onFindUser(KeyEvent key) {
        if (key.getCode().equals(KeyCode.ENTER) && !findUser.getText().isBlank()) {
            list.getChildren().removeIf(el -> el.getId() != null && el.getId().equals("errorMsg"));
            try {
                Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                        Method.POST, Constants.START_CHAT_URL + findUser.getText().strip());

                Response response = requester.doRequest();
                if (response.getCode() != 200) {
                    String msg = response.getCode() == 404
                            ? "User not found"
                            : response.getCode() == 409
                            ? "You already have chat with " + findUser.getText()
                            : "Something wrong";
                    response.getErrorMsg().setText(msg);
                    list.getChildren().add(0, response.getErrorMsg());
                    return;
                }

                ChatDto chat = new ObjectMapper().readValue(response.getJson(), ChatDto.class);
                ApplicationContext.addObjectByName("currentChatId", chat.getId());
                Starter.getGlobalScene().assignToRoot("chat");
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @FXML
    public void onGoToProfile() {
        Starter.getGlobalScene().assignToRoot("profile");
    }

    private void init() {
        list.getChildren().clear();
        List<ChatDto> chats = loadChatList();
        chats.forEach(el -> el.getMessages().sort(Comparator.comparing(MessageDto::getTime).reversed()));
        chats.sort(Comparator.comparing(el -> ((ChatDto) el).getMessages().isEmpty()
                ? LocalDateTime.now()
                : ((ChatDto) el).getMessages().get(0).getTime()).reversed());

        String currentNickname = ((UserDto) ApplicationContext.getObjectByName("currentUser")).getNickname();
        List<BorderPane> chatPanes = new ArrayList<>();
        for (ChatDto chatDto : chats) {
            BorderPane chatPane = (BorderPane) Starter.getGlobalScene().loadView("chatListRow");
            if (chatDto.getUserNickname().equals(currentNickname)) {
                chatPane.getStyleClass().removeIf(el -> el.equals("chat-list-row"));
                chatPane.getStyleClass().add("chat-list-row-notes");
            }
            chatPane.getProperties().put("chatId", chatDto.getId());

            String username = chatDto.getUserNickname().equals(currentNickname) ? "My notes" : chatDto.getUserNickname();
            ((HBox) chatPane.getLeft()).getChildren().stream()
                    .filter(el -> el.getId().equals("username") && el instanceof Text)
                    .forEach(el -> ((Text) el).setText(username));

            ((HBox) chatPane.getLeft()).getChildren().stream()
                    .filter(el -> el.getId().equals("avatar") && el instanceof Circle)
                    .forEach(el -> ((Circle) el).setFill(new ImagePattern(new Image(chatDto.getUserAvatar()))));

            chatPanes.add(chatPane);
        }
        list.getChildren().addAll(chatPanes);
    }

    private List<ChatDto> loadChatList() {
        try {
            Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                    Method.GET, Constants.FETCH_CHATS_URL);

            Response response = requester.doRequest();
            if (response.getCode() != 200) {
                response.getErrorMsg().setText("Cannot load your chat list");
                list.getChildren().add(0, response.getErrorMsg());
                return Collections.emptyList();
            }

            return new ObjectMapper().readValue(response.getJson(), new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
