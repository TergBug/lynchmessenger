package com.messenger.lynch.client.controller.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import javafx.scene.text.Text;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class Requester {

    private final CookieStore cookieStore;
    private final HttpRequestBase request;

    public Requester(CookieStore cookieStore, Method method, String url) {
        this.cookieStore = cookieStore;
        this.request = toHttpRequest(method, url);
    }

    public Response doRequestWithObject(Object obj) {
        if (!(request instanceof HttpEntityEnclosingRequestBase)) {
            throw new RuntimeException("Incorrect method");
        }
        try (CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(cookieStore).build()) {
            StringEntity entity = new StringEntity(new ObjectMapper().writeValueAsString(obj), StandardCharsets.UTF_8);
            entity.setContentType("application/json; charset=utf-8");
            ((HttpEntityEnclosingRequestBase) request).setEntity(entity);
            request.setHeader("Accept", "application/json");
            request.setHeader("Accept-Charset", "utf-8");
            request.setHeader("Content-Type", "application/json; charset=utf-8");

            return createResponse(client);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Response doRequestWithParams(List<NameValuePair> params) {
        if (!(request instanceof HttpEntityEnclosingRequestBase)) {
            throw new RuntimeException("Incorrect method");
        }
        try (CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(cookieStore).build()) {
            ((HttpEntityEnclosingRequestBase) request).setEntity(new UrlEncodedFormEntity(params));
            return createResponse(client);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public Response doRequest() {
        try (CloseableHttpClient client = HttpClients.custom().setDefaultCookieStore(cookieStore).build()) {
            return createResponse(client);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private HttpRequestBase toHttpRequest(Method method, String url) {
        switch (method) {
            case POST:
                return new HttpPost(url);
            case PUT:
                return new HttpPut(url);
            case DELETE:
                return new HttpDelete(url);
            default:
                return new HttpGet(url);
        }
    }

    private Text getErrorMsg() {
        Text errorMsg = new Text();
        errorMsg.setStyle("-fx-fill:#f60000; -fx-font-size:14px;");
        errorMsg.setId("errorMsg");
        return errorMsg;
    }

    private Response createResponse(CloseableHttpClient client) throws Exception {
        CloseableHttpResponse response = client.execute(request);
        String json = response.getEntity() == null || response.getEntity().getContent() == null
                ? ""
                : new String(response.getEntity().getContent().readAllBytes());
        return new Response(response.getStatusLine().getStatusCode(), getErrorMsg(), json);
    }

}
