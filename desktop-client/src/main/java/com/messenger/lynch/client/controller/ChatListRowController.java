package com.messenger.lynch.client.controller;

import com.messenger.lynch.client.Starter;
import com.messenger.lynch.client.constant.ApplicationContext;
import com.messenger.lynch.client.constant.Constants;
import com.messenger.lynch.client.controller.util.Method;
import com.messenger.lynch.client.controller.util.Requester;
import com.messenger.lynch.client.controller.util.Response;
import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;

public class ChatListRowController {

    @FXML
    private BorderPane chatLine;

    @FXML
    public void onDeleteChat() {
        if (!chatLine.getProperties().containsKey("chatId")
                || chatLine.getProperties().get("chatId").toString().isBlank()) {
            return;
        }

        Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                Method.DELETE, Constants.DELETE_CHAT_URL + chatLine.getProperties().get("chatId"));

        Response response = requester.doRequest();
        if (response.getCode() != 200) {
            response.getErrorMsg().setText("Cannot delete the chat");
            chatLine.setBottom(response.getErrorMsg());
            return;
        }

        Starter.getGlobalScene().assignToRoot("chatList");
    }

    @FXML
    public void onOpenChat() {
        ApplicationContext.addObjectByName("currentChatId", chatLine.getProperties().get("chatId"));
        Starter.getGlobalScene().assignToRoot("chat");
    }

}
