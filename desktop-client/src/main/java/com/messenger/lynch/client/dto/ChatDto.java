package com.messenger.lynch.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties
public class ChatDto {

    private String id;

    private String currentUserNickname;

    private String userNickname;

    private String userName;

    private String userAvatar;

    private List<MessageDto> messages;

    public ChatDto() {
    }

    public ChatDto(String id, String currentUserNickname, String userNickname, String userName, String userAvatar,
                   List<MessageDto> messages) {
        this.id = id;
        this.currentUserNickname = currentUserNickname;
        this.userNickname = userNickname;
        this.userName = userName;
        this.userAvatar = userAvatar;
        this.messages = messages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCurrentUserNickname() {
        return currentUserNickname;
    }

    public void setCurrentUserNickname(String currentUserNickname) {
        this.currentUserNickname = currentUserNickname;
    }

    public String getUserNickname() {
        return userNickname;
    }

    public void setUserNickname(String userNickname) {
        this.userNickname = userNickname;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public List<MessageDto> getMessages() {
        return messages;
    }

    public void setMessages(List<MessageDto> messages) {
        this.messages = messages;
    }

}
