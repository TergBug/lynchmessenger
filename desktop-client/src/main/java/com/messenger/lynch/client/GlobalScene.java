package com.messenger.lynch.client;

import com.messenger.lynch.client.exception.FxmlNotFoundException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;

public class GlobalScene {

    private final Stage window;

    public GlobalScene(Stage stage) {
        window = stage;
        window.setTitle("Lynch Messenger");
        window.setScene(new Scene((Parent) loadView("login")));
        window.getIcons().add(new Image(GlobalScene.class.getResource("view/img/mainbg.jpg").toString()));
        window.show();
    }

    public void assignToRoot(String view) {
        window.getScene().setRoot((Parent) loadView(view));
    }

    public Stage getWindow() {
        return window;
    }

    public Node loadView(String view) {
        try {
            return FXMLLoader.load(GlobalScene.class.getResource("view/" + view + ".fxml"));
        } catch (IOException e) {
            throw new FxmlNotFoundException("There is no such view " + view + " or some other problem: " + e);
        }
    }

}
