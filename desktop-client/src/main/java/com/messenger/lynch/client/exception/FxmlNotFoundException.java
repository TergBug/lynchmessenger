package com.messenger.lynch.client.exception;

public class FxmlNotFoundException extends RuntimeException {

    public FxmlNotFoundException(String message) {
        super(message);
    }

}
