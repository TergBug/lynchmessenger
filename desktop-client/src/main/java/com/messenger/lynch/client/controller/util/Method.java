package com.messenger.lynch.client.controller.util;

public enum Method {

    GET, POST, PUT, DELETE

}
