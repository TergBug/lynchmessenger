package com.messenger.lynch.client;

import com.messenger.lynch.client.constant.Constants;
import javafx.application.Application;
import javafx.stage.Stage;

import java.util.Arrays;

public class Starter extends Application {

    private static GlobalScene globalScene;

    public static GlobalScene getGlobalScene() {
        return globalScene;
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("file.encoding"));
        if (args != null && args.length != 0) {
            Arrays.stream(args)
                    .filter(el -> el.matches("^--server=.*$"))
                    .map(el -> el.replace("--server=", ""))
                    .forEach(Constants::refreshUrls);
        }
        launch(args);
    }

    @Override
    public void start(Stage stage) {
        globalScene = new GlobalScene(stage);
    }

}
