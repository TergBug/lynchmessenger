package com.messenger.lynch.client.controller;

import com.messenger.lynch.client.Starter;
import com.messenger.lynch.client.constant.ApplicationContext;
import com.messenger.lynch.client.constant.Constants;
import com.messenger.lynch.client.controller.util.Method;
import com.messenger.lynch.client.controller.util.Requester;
import com.messenger.lynch.client.controller.util.Response;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;
import org.apache.http.client.CookieStore;
import org.apache.http.impl.client.BasicCookieStore;

import java.net.URL;
import java.util.ResourceBundle;

public class MessageController implements Initializable {

    @FXML
    private BorderPane messageLine;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        init();
    }

    private void init() {
        ContextMenu contextMenu = new ContextMenu();
        MenuItem deleteMenuItem = new MenuItem("Delete");
        deleteMenuItem.setOnAction(event -> onClickDelete());
        contextMenu.getItems().addAll(deleteMenuItem);
        messageLine.setOnContextMenuRequested(event -> contextMenu.show(messageLine, event.getScreenX(), event.getScreenY()));
    }

    private void onClickDelete() {
        if (!messageLine.getProperties().containsKey("messageId")
                || messageLine.getProperties().get("messageId").toString().isBlank()) {
            return;
        }

        Requester requester = new Requester((CookieStore) ApplicationContext.getObject(BasicCookieStore.class),
                Method.DELETE, Constants.DELETE_MESSAGE_URL + messageLine.getProperties().get("messageId"));

        Response response = requester.doRequest();
        if (response.getCode() != 200) {
            response.getErrorMsg().setText("Cannot delete the chat");
            messageLine.setBottom(response.getErrorMsg());
            return;
        }

        Starter.getGlobalScene().assignToRoot("chat");
    }

}
