package com.messenger.lynch.client.constant;

public final class Constants {

    public static final String DEFAULT_AVATAR = "https://upload.wikimedia.org/wikipedia/commons/9/99/Sample_User_Icon.png";

    public static String TIMEZONE;

    public static String SERVER_URL;
    public static String SIGNUP_URL;
    public static String LOGIN_URL;
    public static String LOGOUT_URL;
    public static String UPDATE_PROFILE_URL;
    public static String FETCH_CHATS_URL;
    public static String FETCH_MESSAGES_URL;
    public static String ADD_MESSAGE_URL;
    public static String START_CHAT_URL;
    public static String DELETE_CHAT_URL;
    public static String DELETE_MESSAGE_URL;

    static {
        refreshUrls("localhost");
        TIMEZONE = "Europe/Kiev";
    }

    public static void refreshUrls(String hostName) {
        SERVER_URL = "http://" + hostName + ":8080/";
        SIGNUP_URL = SERVER_URL + "users/register";
        LOGIN_URL = SERVER_URL + "users/login";
        LOGOUT_URL = SERVER_URL + "users/logout";
        UPDATE_PROFILE_URL = SERVER_URL + "users/update";
        FETCH_CHATS_URL = SERVER_URL + "chats/fetch";
        FETCH_MESSAGES_URL = SERVER_URL + "messages/fetch/";
        ADD_MESSAGE_URL = SERVER_URL + "messages/add";
        START_CHAT_URL = SERVER_URL + "chats/start/";
        DELETE_CHAT_URL = SERVER_URL + "chats/delete/";
        DELETE_MESSAGE_URL = SERVER_URL + "messages/delete/";
    }

}
