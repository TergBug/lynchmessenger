package com.messenger.lynch.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.Set;

@Data
@SuperBuilder
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ChatDto {

    private String id;

    private String currentUserNickname;

    private String userNickname;

    private String userName;

    private String userAvatar;

    private Set<MessageDto> messages;

}
