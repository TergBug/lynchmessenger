package com.messenger.lynch.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageDto {

    private String id;

    private String userNickname;

    private String userName;

    private String userAvatar;

    private String chatId;

    private LocalDateTime time;

    private String text;

}
