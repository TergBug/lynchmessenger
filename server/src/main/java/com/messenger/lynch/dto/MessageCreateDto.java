package com.messenger.lynch.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Data
@SuperBuilder
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MessageCreateDto {

    @NotBlank(message = "Chat ID must not be blank!")
    private String chatId;

    @NotBlank(message = "Time zone must not be blank!")
    private String timeZone;

    @NotBlank(message = "Text must not be blank!")
    private String text;

}
