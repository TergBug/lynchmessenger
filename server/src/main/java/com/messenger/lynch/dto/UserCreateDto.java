package com.messenger.lynch.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.validation.constraints.NotBlank;

@Data
@SuperBuilder
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserCreateDto {

    @NotBlank(message = "Nickname must not be a blank!")
    private String nickname;

    private String name;

    @NotBlank(message = "Password must not be a blank!")
    private String password;

    private String avatarLink;

}
