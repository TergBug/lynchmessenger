package com.messenger.lynch.filters;

import com.messenger.lynch.dto.UserCallbackDto;
import com.messenger.lynch.dto.UserSignInDto;
import com.messenger.lynch.exception.AuthenticationException;
import com.messenger.lynch.facade.UserFacade;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

@Order(1)
@Component
@RequiredArgsConstructor
public class AuthenticationFilter extends OncePerRequestFilter {

    private static final String LOGIN_ENDPOINT = "/users/login";

    private final UserFacade userFacade;

    @Value("${security.freeAuthorityEndpoints}")
    private String[] freeEndpoints;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String path = request.getServletPath();
        String method = request.getMethod();

        if (HttpMethod.POST.matches(method) && path.equals(LOGIN_ENDPOINT)) {
            doLoginPost(request);
        }
        if (!Arrays.asList(freeEndpoints).contains(path)) {
            doOnNotFreeEndpoints(request);
        }

        filterChain.doFilter(request, response);
    }

    private void doLoginPost(HttpServletRequest request) {
        String nickname = request.getParameter("nickname");
        String password = request.getParameter("password");
        UserSignInDto userSignInDto = userSignInDtoFromRequest(nickname, password);
        userFacade.signIn(userSignInDto);

        request.getSession().setAttribute("nickname", nickname);
        userFacade.authenticate(userSignInDto);
    }

    private void doOnNotFreeEndpoints(HttpServletRequest request) {
        String userNickname = (String) request.getSession().getAttribute("nickname");

        if (userNickname != null) {
            UserCallbackDto user = userFacade.getUserByNickname(userNickname);
            userFacade.authenticate(userSignInDtoFromRequest(user.getNickname()));
        }
    }

    private UserSignInDto userSignInDtoFromRequest(String nickname, String password) {
        if (nickname == null || nickname.isBlank() || password == null || password.isBlank()) {
            throw new AuthenticationException();
        }
        return UserSignInDto.builder()
                .nickname(nickname)
                .password(password)
                .build();
    }

    private UserSignInDto userSignInDtoFromRequest(String nickname) {
        if (nickname == null || nickname.isBlank()) {
            throw new AuthenticationException();
        }
        return UserSignInDto.builder()
                .nickname(nickname)
                .password("")
                .build();
    }

}
