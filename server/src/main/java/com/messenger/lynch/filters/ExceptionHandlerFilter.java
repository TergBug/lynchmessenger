package com.messenger.lynch.filters;

import com.messenger.lynch.exception.AuthenticationException;
import com.messenger.lynch.exception.InvalidTokenException;
import com.messenger.lynch.exception.UserNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Order(0)
@Component
@AllArgsConstructor
public class ExceptionHandlerFilter extends OncePerRequestFilter {

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                 FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (UserNotFoundException | InvalidTokenException | AuthenticationException e) {
            setErrorResponse(request, response, e);
        }
    }

    private void setErrorResponse(HttpServletRequest request, HttpServletResponse response,
                                  Exception ex) throws IOException, ServletException {
        request.setAttribute("errorName", ex.getClass().getSimpleName());
        if (ex instanceof UserNotFoundException || ex instanceof AuthenticationException) {
            request.setAttribute("message", "Invalid nickname or password");
        } else {
            request.setAttribute("message", ex.getMessage());
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(request.getContextPath() + "/users/loginError");
        dispatcher.forward(request, response);
    }

}
