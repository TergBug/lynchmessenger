package com.messenger.lynch.service;

import com.messenger.lynch.dto.UserSignInDto;

public interface AuthenticationService {

    void authenticateUser(UserSignInDto user);

}
