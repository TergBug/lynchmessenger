package com.messenger.lynch.service.impl;

import com.messenger.lynch.exception.MessageNotFoundException;
import com.messenger.lynch.exception.WrongMessageException;
import com.messenger.lynch.model.Message;
import com.messenger.lynch.model.User;
import com.messenger.lynch.repository.MessageRepository;
import com.messenger.lynch.service.MessageService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    public Message add(Message message) {
        String ownerNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();

        if (!message.getChat().getUser1().getNickname().equals(ownerNickname)
                && !message.getChat().getUser2().getNickname().equals(ownerNickname)) {
            throw new WrongMessageException("Owner (" + ownerNickname + ") of the message should take part in the chat");
        }
        message.setUser(User.builder().nickname(ownerNickname).build());
        return messageRepository.save(message);
    }

    @Override
    public void deleteById(String id) {
        String currentUserNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Message message = messageRepository.findById(id)
                .orElseThrow(() -> new MessageNotFoundException("No message with id " + id));
        if (!message.getChat().getUser1().getNickname().equals(currentUserNickname)
                && !message.getChat().getUser2().getNickname().equals(currentUserNickname)) {
            throw new MessageNotFoundException("User " + currentUserNickname + " cannot delete message with id " + id);
        }
        messageRepository.deleteById(id);
    }

}
