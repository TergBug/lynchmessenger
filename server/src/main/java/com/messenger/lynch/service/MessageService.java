package com.messenger.lynch.service;

import com.messenger.lynch.model.Message;

public interface MessageService {

    Message add(Message message);

    void deleteById(String id);

}
