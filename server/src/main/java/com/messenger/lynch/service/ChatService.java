package com.messenger.lynch.service;

import com.messenger.lynch.model.Chat;

import java.util.List;

public interface ChatService {

    Chat save(Chat chat);

    Chat findById(String id);

    List<Chat> findAllByUserNickname(String userNickname);

    void deleteById(String id);

}
