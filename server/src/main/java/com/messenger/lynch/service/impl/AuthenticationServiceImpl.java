package com.messenger.lynch.service.impl;

import com.messenger.lynch.dto.UserSignInDto;
import com.messenger.lynch.service.AuthenticationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    @Override
    public void authenticateUser(UserSignInDto user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("user"));
        String password = user.getPassword() != null ? user.getPassword() : "";

        UserDetails userDetails = new User(user.getNickname(), password, authorities);
        Authentication authentication = new UsernamePasswordAuthenticationToken(userDetails, password, authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        log.info("User with nickname " + user.getNickname() + " has authenticated");
    }

}
