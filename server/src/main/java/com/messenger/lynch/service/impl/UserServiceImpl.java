package com.messenger.lynch.service.impl;

import com.messenger.lynch.exception.UserAlreadyExistsException;
import com.messenger.lynch.exception.UserNotFoundException;
import com.messenger.lynch.model.User;
import com.messenger.lynch.repository.UserRepository;
import com.messenger.lynch.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public void save(User user) {
        if (userRepository.findById(user.getNickname()).isPresent()) {
            throw new UserAlreadyExistsException("User with nickname " + user.getNickname() + " already exists");
        }
        userRepository.save(user);
    }

    @Override
    public User update(User user) {
        User userToUpdate = userRepository.findById(user.getNickname())
                .orElseThrow(() -> new UserNotFoundException("There is no user with nickname " + user.getNickname()));
        userToUpdate.setNickname(user.getNickname());
        userToUpdate.setName(user.getName());
        userToUpdate.setAvatarLink(user.getAvatarLink());
        return userRepository.save(userToUpdate);
    }

    @Override
    public User getUserByNickname(String nickname) {
        return userRepository.findById(nickname)
                .orElseThrow(() -> new UserNotFoundException("No user with nickname " + nickname));
    }

}
