package com.messenger.lynch.service;

import com.messenger.lynch.model.User;

public interface UserService {

    void save(User user);

    User update(User user);

    User getUserByNickname(String nickname);

}
