package com.messenger.lynch.service.impl;

import com.messenger.lynch.exception.ChatNotFoundException;
import com.messenger.lynch.exception.DuplicateChatException;
import com.messenger.lynch.model.Chat;
import com.messenger.lynch.repository.ChatRepository;
import com.messenger.lynch.service.ChatService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@AllArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final ChatRepository chatRepository;

    @Override
    public Chat save(Chat chat) {
        Optional<Chat> existedChat = chatRepository.findChatByUser1AndUser2(chat.getUser1(), chat.getUser2())
                .or(() -> chatRepository.findChatByUser1AndUser2(chat.getUser2(), chat.getUser1()));
        if (existedChat.isPresent()) {
            throw new DuplicateChatException("The chat with user " + chat.getUser1().getNickname() + " and user "
                    + chat.getUser2().getNickname() + " already exists");
        }
        return chatRepository.save(chat);
    }

    @Override
    public Chat findById(String id) {
        String currentUserNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Chat chat = chatRepository.findById(id)
                .orElseThrow(() -> new ChatNotFoundException("No chat with id " + id));
        if (!chat.getUser1().getNickname().equals(currentUserNickname)
                && !chat.getUser2().getNickname().equals(currentUserNickname)) {
            throw new ChatNotFoundException("User " + currentUserNickname + " cannot get chat with id " + id);
        }
        return chat;
    }

    @Override
    public List<Chat> findAllByUserNickname(String userNickname) {
        return chatRepository.findAllByUserNickname(userNickname);
    }

    @Override
    public void deleteById(String id) {
        String currentUserNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        Chat chat = chatRepository.findById(id).orElseThrow(() -> new ChatNotFoundException("No chat with id " + id));
        if (!chat.getUser1().getNickname().equals(currentUserNickname)
                && !chat.getUser2().getNickname().equals(currentUserNickname)) {
            throw new ChatNotFoundException("User " + currentUserNickname + " cannot delete chat with id " + id);
        }
        chatRepository.deleteById(id);
    }

}
