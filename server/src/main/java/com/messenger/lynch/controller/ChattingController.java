package com.messenger.lynch.controller;

import com.messenger.lynch.dto.ChatDto;
import com.messenger.lynch.facade.ChatFacade;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/chats")
public class ChattingController {

    private final ChatFacade chatFacade;

    @GetMapping("/fetch")
    public ResponseEntity<List<ChatDto>> fetchChatsByNickname() {
        return new ResponseEntity<>(chatFacade.fetchAllChatsForUser(), HttpStatus.OK);
    }

    @PostMapping("/start/{nickname}")
    public ResponseEntity<ChatDto> startChatWithUser(@PathVariable String nickname) {
        return new ResponseEntity<>(chatFacade.startChatWith(nickname), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteChatById(@PathVariable String id) {
        chatFacade.deleteChatById(id);
    }

}
