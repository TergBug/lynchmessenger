package com.messenger.lynch.controller;

import com.messenger.lynch.dto.ErrorDto;
import com.messenger.lynch.dto.UserCallbackDto;
import com.messenger.lynch.dto.UserCreateDto;
import com.messenger.lynch.dto.UserUpdateDto;
import com.messenger.lynch.facade.UserFacade;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserFacade userFacade;

    @GetMapping("/find/{nickname}")
    public ResponseEntity<UserCallbackDto> findUser(@PathVariable String nickname) {
        return new ResponseEntity<>(userFacade.getUserByNickname(nickname), HttpStatus.OK);
    }

    @PostMapping("/register")
    public void registerUser(@Valid @RequestBody UserCreateDto user) {
        userFacade.save(user);
    }

    @PostMapping("/login")
    public ResponseEntity<UserCallbackDto> loginUser() {
        String nickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        return new ResponseEntity<>(userFacade.getUserByNickname(nickname), HttpStatus.OK);
    }

    @PutMapping(path = "/update")
    public ResponseEntity<UserCallbackDto> updateUser(@Valid @RequestBody UserUpdateDto user) {
        return new ResponseEntity<>(userFacade.update(user), HttpStatus.OK);
    }

    @RequestMapping(path = "/loginError", method = {RequestMethod.GET, RequestMethod.POST})
    public ResponseEntity<ErrorDto> loginError(HttpServletRequest request) {
        return new ResponseEntity<>(new ErrorDto(request.getAttribute("errorName").toString(),
                request.getAttribute("message").toString()), HttpStatus.FORBIDDEN);
    }

}
