package com.messenger.lynch.controller;

import com.messenger.lynch.dto.ChatDto;
import com.messenger.lynch.dto.MessageCreateDto;
import com.messenger.lynch.facade.ChatFacade;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@AllArgsConstructor
@RequestMapping("/messages")
public class MessageController {

    private final ChatFacade chatFacade;

    @GetMapping("/fetch/{id}")
    public ResponseEntity<ChatDto> fetchChatById(@PathVariable String id) {
        return new ResponseEntity<>(chatFacade.fetchChatById(id), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<ChatDto> addMessage(@Valid @RequestBody MessageCreateDto message) {
        return new ResponseEntity<>(chatFacade.addMessage(message), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteMessageById(@PathVariable String id) {
        chatFacade.deleteMessageById(id);
    }

}
