package com.messenger.lynch.facade;

import com.messenger.lynch.dto.ChatDto;
import com.messenger.lynch.dto.MessageCreateDto;

import java.util.List;

public interface ChatFacade {

    List<ChatDto> fetchAllChatsForUser();

    ChatDto fetchChatById(String id);

    ChatDto startChatWith(String nickname);

    void deleteChatById(String id);

    ChatDto addMessage(MessageCreateDto messageDto);

    void deleteMessageById(String id);

}
