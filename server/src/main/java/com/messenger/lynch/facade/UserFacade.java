package com.messenger.lynch.facade;

import com.messenger.lynch.dto.UserCallbackDto;
import com.messenger.lynch.dto.UserCreateDto;
import com.messenger.lynch.dto.UserSignInDto;
import com.messenger.lynch.dto.UserUpdateDto;

public interface UserFacade {

    UserCallbackDto signIn(UserSignInDto userSignInDto);

    void save(UserCreateDto userCreateDto);

    UserCallbackDto update(UserUpdateDto userUpdateDto);

    UserCallbackDto getUserByNickname(String nickname);

    void authenticate(UserSignInDto user);

}
