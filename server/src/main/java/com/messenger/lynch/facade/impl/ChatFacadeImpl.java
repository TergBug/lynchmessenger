package com.messenger.lynch.facade.impl;

import com.messenger.lynch.dto.ChatDto;
import com.messenger.lynch.dto.MessageCreateDto;
import com.messenger.lynch.facade.ChatFacade;
import com.messenger.lynch.mapper.ChatMapper;
import com.messenger.lynch.mapper.MessageMapper;
import com.messenger.lynch.model.Chat;
import com.messenger.lynch.model.Message;
import com.messenger.lynch.model.User;
import com.messenger.lynch.service.ChatService;
import com.messenger.lynch.service.MessageService;
import com.messenger.lynch.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ChatFacadeImpl implements ChatFacade {

    private final ChatService chatService;
    private final UserService userService;
    private final MessageService messageService;

    private final ChatMapper chatMapper;
    private final MessageMapper messageMapper;

    @Transactional
    @Override
    public List<ChatDto> fetchAllChatsForUser() {
        String currentUserNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getUsername();
        List<Chat> chats = chatService.findAllByUserNickname(currentUserNickname);
        return chats.stream()
                .map(chatMapper::toDto)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public ChatDto fetchChatById(String id) {
        return chatMapper.toDto(chatService.findById(id));
    }

    @Transactional
    @Override
    public ChatDto startChatWith(String nickname) {
        String currentUserNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getUsername();
        User user1 = userService.getUserByNickname(currentUserNickname);
        User user2 = userService.getUserByNickname(nickname);
        Chat newChat = Chat.builder()
                .user1(user1)
                .user2(user2)
                .build();
        return chatMapper.toDto(chatService.save(newChat));
    }

    @Transactional
    @Override
    public void deleteChatById(String id) {
        chatService.deleteById(id);
    }

    @Transactional
    @Override
    public ChatDto addMessage(MessageCreateDto messageDto) {
        Chat chat = chatService.findById(messageDto.getChatId());
        ChatDto outputChat = chatMapper.toDto(chat);

        Message addedMessage = messageService.add(messageMapper.toModel(messageDto, chat));
        addedMessage.setUser(userService.getUserByNickname(addedMessage.getUser().getNickname()));

        outputChat.getMessages().add(messageMapper.toDto(addedMessage));
        return outputChat;
    }

    @Transactional
    @Override
    public void deleteMessageById(String id) {
        messageService.deleteById(id);
    }

}
