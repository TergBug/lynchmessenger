package com.messenger.lynch.facade.impl;

import com.messenger.lynch.dto.UserCallbackDto;
import com.messenger.lynch.dto.UserCreateDto;
import com.messenger.lynch.dto.UserSignInDto;
import com.messenger.lynch.dto.UserUpdateDto;
import com.messenger.lynch.exception.AuthenticationException;
import com.messenger.lynch.facade.UserFacade;
import com.messenger.lynch.mapper.UserMapper;
import com.messenger.lynch.model.User;
import com.messenger.lynch.service.AuthenticationService;
import com.messenger.lynch.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@AllArgsConstructor
public class UserFacadeImpl implements UserFacade {

    private final UserService userService;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;
    private final AuthenticationService authenticationService;

    @Transactional
    @Override
    public UserCallbackDto signIn(UserSignInDto userSignInDto) {
        User user = userService.getUserByNickname(userSignInDto.getNickname());
        if (!passwordEncoder.matches(userSignInDto.getPassword(), user.getPassword())) {
            throw new AuthenticationException("Invalid login or password");
        }
        return userMapper.toDto(user);
    }

    @Transactional
    @Override
    public void save(UserCreateDto userCreateDto) {
        userService.save(userMapper.toModel(userCreateDto));
    }

    @Transactional
    @Override
    public UserCallbackDto update(UserUpdateDto userUpdateDto) {
        String nickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
        userUpdateDto.setNickname(nickname);
        return userMapper.toDto(userService.update(userMapper.toModel(userUpdateDto)));
    }

    @Transactional
    @Override
    public UserCallbackDto getUserByNickname(String nickname) {
        return userMapper.toDto(userService.getUserByNickname(nickname));
    }

    @Transactional
    @Override
    public void authenticate(UserSignInDto user) {
        authenticationService.authenticateUser(user);
    }

}
