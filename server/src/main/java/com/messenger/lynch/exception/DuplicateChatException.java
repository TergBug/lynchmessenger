package com.messenger.lynch.exception;

public class DuplicateChatException extends RuntimeException {

    public DuplicateChatException(String message) {
        super(message);
    }

}
