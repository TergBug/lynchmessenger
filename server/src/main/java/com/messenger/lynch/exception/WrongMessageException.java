package com.messenger.lynch.exception;

public class WrongMessageException extends RuntimeException {

    public WrongMessageException(String message) {
        super(message);
    }

}
