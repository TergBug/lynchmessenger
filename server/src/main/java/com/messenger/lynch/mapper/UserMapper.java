package com.messenger.lynch.mapper;

import com.messenger.lynch.dto.UserCallbackDto;
import com.messenger.lynch.dto.UserCreateDto;
import com.messenger.lynch.dto.UserUpdateDto;
import com.messenger.lynch.mapper.util.MappingUtil;
import com.messenger.lynch.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserMapper {

    private final PasswordEncoder passwordEncoder;

    @Value("${constant.default.avatarUrl}")
    private String defaultAvatarUrl;

    public UserCallbackDto toDto(User user) {
        return UserCallbackDto.builder()
                .nickname(user.getNickname())
                .name(user.getName())
                .avatarLink(MappingUtil.getAvatarUrl(user.getAvatarLink(), defaultAvatarUrl))
                .build();
    }

    public User toModel(UserCreateDto user) {
        return User.builder()
                .nickname(user.getNickname())
                .name(getName(user.getName(), user.getNickname()))
                .password(passwordEncoder.encode(user.getPassword()))
                .avatarLink(MappingUtil.getAvatarUrl(user.getAvatarLink(), defaultAvatarUrl))
                .build();
    }

    public User toModel(UserUpdateDto user) {
        return User.builder()
                .nickname(user.getNickname())
                .name(getName(user.getName(), user.getNickname()))
                .avatarLink(MappingUtil.getAvatarUrl(user.getAvatarLink(), defaultAvatarUrl))
                .build();
    }

    private String getName(String name, String nickname) {
        return (name == null || name.isBlank())
                ? nickname
                : name;
    }

}
