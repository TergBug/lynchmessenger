package com.messenger.lynch.mapper;

import com.messenger.lynch.dto.MessageCreateDto;
import com.messenger.lynch.dto.MessageDto;
import com.messenger.lynch.mapper.util.MappingUtil;
import com.messenger.lynch.model.Chat;
import com.messenger.lynch.model.Message;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;

@Component
@RequiredArgsConstructor
public class MessageMapper {

    @Value("${constant.default.avatarUrl}")
    private String defaultAvatarUrl;

    public MessageDto toDto(Message message) {
        return MessageDto.builder()
                .id(message.getId())
                .chatId(message.getChat().getId())
                .userNickname(message.getUser().getNickname())
                .userName(message.getUser().getName())
                .userAvatar(MappingUtil.getAvatarUrl(message.getUser().getAvatarLink(), defaultAvatarUrl))
                .time(message.getTime())
                .text(message.getText())
                .build();
    }

    public Message toModel(MessageCreateDto message, Chat chat) {
        return Message.builder()
                .chat(chat)
                .time(LocalDateTime.now(ZoneId.of(message.getTimeZone())))
                .text(message.getText())
                .build();
    }

}
