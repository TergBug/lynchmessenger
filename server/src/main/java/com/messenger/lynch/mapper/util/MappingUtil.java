package com.messenger.lynch.mapper.util;

public class MappingUtil {

    private static final String AVATAR_LINK_REGEX = "^http(s)?://.*\\.(gif|GIF|jpg|JPG|jpeg|JPEG|png|PNG)$";

    public static String getAvatarUrl(String avatarUrl, String defaultAvatarUrl) {
        return (avatarUrl == null || !avatarUrl.matches(AVATAR_LINK_REGEX))
                ? defaultAvatarUrl
                : avatarUrl;
    }

}
