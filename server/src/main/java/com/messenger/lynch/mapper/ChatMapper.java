package com.messenger.lynch.mapper;

import com.messenger.lynch.dto.ChatDto;
import com.messenger.lynch.dto.MessageDto;
import com.messenger.lynch.mapper.util.MappingUtil;
import com.messenger.lynch.model.Chat;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class ChatMapper {

    @Value("${constant.default.avatarUrl}")
    private String defaultAvatarUrl;

    private final MessageMapper messageMapper;

    public ChatDto toDto(Chat chat) {
        String currentUserNickname = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal())
                .getUsername();
        Set<MessageDto> messages = chat.getMessages() == null ? null : chat.getMessages().stream()
                .map(messageMapper::toDto)
                .collect(Collectors.toSet());
        return ChatDto.builder()
                .id(chat.getId())
                .currentUserNickname(currentUserNickname)
                .userNickname(chat.getUser1().getNickname().equals(currentUserNickname)
                        ? chat.getUser2().getNickname()
                        : chat.getUser1().getNickname())
                .userName(chat.getUser1().getNickname().equals(currentUserNickname)
                        ? chat.getUser2().getName()
                        : chat.getUser1().getName())
                .userAvatar(MappingUtil.getAvatarUrl(chat.getUser1().getNickname().equals(currentUserNickname)
                        ? chat.getUser2().getAvatarLink()
                        : chat.getUser1().getAvatarLink(), defaultAvatarUrl))
                .messages(messages)
                .build();
    }

}
