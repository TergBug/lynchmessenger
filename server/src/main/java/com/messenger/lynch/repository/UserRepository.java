package com.messenger.lynch.repository;

import com.messenger.lynch.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, String> {

}
