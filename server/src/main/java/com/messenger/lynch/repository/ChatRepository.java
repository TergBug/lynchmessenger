package com.messenger.lynch.repository;

import com.messenger.lynch.model.Chat;
import com.messenger.lynch.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ChatRepository extends JpaRepository<Chat, String> {

    Optional<Chat> findChatByUser1AndUser2(User user1, User user2);

    @Query("SELECT DISTINCT ch FROM Chat ch JOIN FETCH ch.user1 u1 JOIN FETCH ch.user2 u2 LEFT JOIN FETCH ch.messages m "
            + "WHERE u1.nickname = ?1 OR u2.nickname = ?1")
    List<Chat> findAllByUserNickname(String nickname);

}
