package com.messenger.lynch.repository;

import com.messenger.lynch.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, String> {

}
