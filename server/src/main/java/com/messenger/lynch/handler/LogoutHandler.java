package com.messenger.lynch.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@Slf4j
@Component
public class LogoutHandler implements LogoutSuccessHandler {

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException {
        if (authentication == null) {
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.getWriter().write(new ObjectMapper().createObjectNode()
                    .put("timestamp", LocalDateTime.now().toString())
                    .put("status", HttpStatus.FORBIDDEN.value())
                    .put("error", "Forbidden")
                    .put("message", "Access denied")
                    .put("path", request.getServletPath())
                    .toPrettyString());
            return;
        }
        log.info("User " + ((UserDetails) authentication.getPrincipal()).getUsername() + " has quited the system");
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

}
