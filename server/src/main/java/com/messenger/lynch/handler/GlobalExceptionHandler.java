package com.messenger.lynch.handler;

import com.messenger.lynch.dto.ErrorDto;
import com.messenger.lynch.exception.ChatNotFoundException;
import com.messenger.lynch.exception.DuplicateChatException;
import com.messenger.lynch.exception.MessageNotFoundException;
import com.messenger.lynch.exception.UserAlreadyExistsException;
import com.messenger.lynch.exception.UserNotFoundException;
import com.messenger.lynch.exception.WrongMessageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({UserNotFoundException.class, ChatNotFoundException.class, MessageNotFoundException.class})
    public final ResponseEntity<ErrorDto> handleNotFoundException(Exception ex) {
        log.warn(ex.getMessage());
        return new ResponseEntity<>(new ErrorDto(ex.getClass().getSimpleName(), ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler({DuplicateChatException.class, WrongMessageException.class, UserAlreadyExistsException.class})
    public final ResponseEntity<ErrorDto> handleDuplicateException(Exception ex) {
        log.warn(ex.getMessage());
        return new ResponseEntity<>(new ErrorDto(ex.getClass().getSimpleName(), ex.getMessage()), HttpStatus.CONFLICT);
    }

}
