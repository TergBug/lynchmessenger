package com.messenger.lynch.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Entity
@Table(name = "users")
@Data
@SuperBuilder
@NoArgsConstructor
public class User {

    @Id
    private String nickname;

    private String name;

    private String password;

    @Column(name = "avatar_link")
    private String avatarLink;

    @OneToMany(mappedBy = "user1")
    private List<Chat> chats1;

    @OneToMany(mappedBy = "user2")
    private List<Chat> chats2;

    public List<Chat> getAllChats() {
        return Stream.concat(chats1.stream(), chats2.stream()).collect(Collectors.toList());
    }

}
