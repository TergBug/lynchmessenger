<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title> Lynch </title>
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/chatList.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/chatList.css">
    <script type="text/javascript">
        function hideEdit() {
            var b = document.getElementById("profileEdit");
            b.style.display = "none";
            var a = document.getElementById("allExceptForEdit");
            //a.style.display = "none";
        }

        function hide() {
            var profileEdit = document.getElementById("profileEdit");
            var chatList = document.getElementById("chatList");
            var image = document.getElementById("headerButton");
            var searchBox = document.getElementById("searchBox");
            if (image.getAttribute('src') === 'pics/functional/rotated.png') {
                image.src = "pics/functional/menu.png";
            } else {
                image.src = "pics/functional/rotated.png";
            }
            if (profileEdit.style.display == "none") {
                profileEdit.style.display = "block";
            } else {
                profileEdit.style.display = "none";
            }
            if (chatList.style.display == "none") {
                chatList.style.display = "block";
                searchBox.style.display = "block";
            } else {
                chatList.style.display = "none";
                searchBox.style.display = "none";
            }
        }
        document.onkeydown = function(evt) {
            var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
            if (keyCode == 13) {
                //your function call here
                document.findForm.submit();
            }
        }
    </script>
</head>

<?php
    session_start();
    $_SESSION['history'] = $_SERVER['REQUEST_URI'];
    if ($_SESSION['jid'] == ""){
        header("Location: index.php");
    }
require 'vendor/autoload.php';
require 'services/fetchChats.php';
?>

<body onload="hideEdit()">
    <div class="side-col"></div>
    <div class="mid-col noselect">
        <div class="header noselect">
            <div class="menu-button"> <img class="noselect" onclick="hide()" id="headerButton" src="pics/functional/menu.png"> </div>
            <div class="title noselect">LynchMessenger</div>
        </div>
        <div id="profileEdit" class="edit">
            <br>
            <br>
            <div class="profilePictureSideFiller"> </div>
            <div class="profilePicture" style="background-image:url(<?php echo $_SESSION['user']['avatarLink'] ?> );"> </div>
            <div class="profilePictureSideFiller">
                <form action="services/deleteAvatar.php" method="post">

                    <input type="text" name="name" value="<?php echo $_SESSION['user']['name'] ?>" hidden>
                    <input type="image" class="pictureFunctions" src="pics/functional/delete.PNG">
                </form>
            </div>
            <form method="post" action="services/updateUser.php">
                <input type="text" hidden name="mId" value="chatList">
                <div class="datas">
                    <div class="profileInfo">
                        <div id="profileDataHead" class="profileData noselect"> Nickname [fixed]</div>
                        <div id="profileDataValue" class="profileData">
                            <input id="profileData" type="text" readonly value="<?php echo $_SESSION['user']['nickname'] ?>">
                        </div>
                        <div id="profileDataHead" class="profileData noselect"> Public Name [editable]</div>
                        <div id="profileDataValue" class="profileData">
                            <input name="name" id="profileData" type="text" value="<?php echo $_SESSION['user']['name'] ?>">
                        </div>
                        <div id="profileDataHead" class="profileData noselect"> Profile picture [link] </div>
                        <div id="profileDataValue" class="profileData">
                            <input name="link" id="profileDataLink" type="text" value="<?php echo $_SESSION['user']['avatarLink'] ?>">
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
                <div class="submit">
                    <button id="submitEdit" type="submit">SUBMIT</button>
                </div>
            </form>
            <div class="logout">
                <a href="services/logout.php">
                    <input id="logoutButton" type="submit" value="Logout"> </a>
            </div>
        </div>

        <div id="searchBox" class="searcherBox">
            <form name="findForm" action="services/startChat.php" method="post">
                <input id="search" name="find" type="text" value="" placeholder="find a user by username">
            </form>
        </div>
        <?php
error_reporting(0);
ini_set('display_errors', 0);

        if ($_SESSION['err']){
            echo   "    <div id='chatBoxError' class='chatBox'>
                        <div id='errorMessage' class='chatBoxUser'>
                           ".$_SESSION['err']."
                        </div>

                </div> ";
        }else{        }
    ?>
        <div id='chatList' class='chatList'>
                <?php
                     $id = 0;
                    foreach ($_SESSION['chat'] as $a) {
    echo "<div id='chatBoxBg1' class='chatBox'>
                <a href='chatMessage?id=".$id."'>
                    <div class='chatBoxPfp' style='background-image: url(" . $a['userAvatar'] . ")'> </div>
                </a>
                <a href='chatMessage?id=".$id."'>
                    <div class='chatBoxUser'> " . $a['userNickname'] .  " </div>
                </a>
                    <div class='chatBoxDelete'>
                        <form name='deleteForm' method='post' action='services/deleteChat.php'>
                            <input type='text' name='chatId' hidden value='". $a['id'] ."'>
                            <input type='image' id='submitImageDelete' src='pics/functional/delete.png'>
                        </form>
                    </div>
           </div>" ;
    $id++;
}
    ?>
        </div>
    </div>
    <div class="side-col"></div>
</body>
</html>
