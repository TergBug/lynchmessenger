Nikolaienko Artem's branch on
<h1>Lynch Messenger</h1> 

Team of 122-19sk-1 group

Members: <br>
Zaitsev Maxim,  <br>
Nikolaienko Artem, <br>
Ruksov Yevhenii.

<i> Folders: </i><br>
<b> /css </b> is for css the style files <br>
<b> /pics </b> is for images that r gonna be used  <br>
<b> /services </b> is for processing files such as get/post methods and server-requests <br>
<b> /vendor </b> contains <b>Composer</b> and <b>Guzzle</b> libraries <br>
<b>Composer</b> is an application-level package manager for the PHP programming language that provides a standard format for managing dependencies of PHP software and required libraries. <br>
<b>Guzzle</b> is a HTTP client library for PHP. Initially a wrapper library around cURL, it evolved to a transport agnostic PSR-7 compatible library.

<b>index.php</b> file is only used to redirect client to /views pages