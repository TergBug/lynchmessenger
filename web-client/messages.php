<head>
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/chatList.css">
    <link rel="stylesheet" href="../css/chatMessage.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/chatList.css">
    <link rel="stylesheet" href="css/chatMessage.css">

<script type="text/javascript">
    function scroll(){
        alert("nig");
        window.scrollTo(10,2000);
        document.scrollTo(10,2000);
    };
</script>
</head>

<?php
session_start();
$_SESSION['history'] = $_SERVER['REQUEST_URI'];
require 'vendor/autoload.php';
require 'services/fetchChats.php';
$id = $_GET['id'];

usort($_SESSION['chat'][$id]['messages'], function ($item1, $item2) {
    return $item1['time']<=>$item2['time'];
});

foreach ($_SESSION['chat'][$id]['messages'] as $msgs) {
    $d = date_create($msgs['time']);
    $cc = (str_replace("\r\n", '<br>', $msgs['text']));
    echo "<br>";
    echo "
       <div onload='scroll();' class='singleMessage'>
            <div class='singleMessageSideCol noselect'>
                    <div class='singleMessagePfp' style='background-image: url(" . $msgs['userAvatar'] . ")'></div>
            </div>
            <div class='singleMessageMidCol'>
                    <div class='singleMessageUsername'>" . $msgs['userName'] . "</div>
                    <div class='singleMessageText'>" . $cc . " </div>
            </div>
            <div class='singleMessageSideCol noselect'>
                    <div class='singleMessageDate'>
                         " . date_format($d, 'Y-m-d H:i:s') . "
                        <form action='services/deleteMessage.php' method='post'>
                            <input id='deleteMessage' src='pics/functional/delete.png' type='image'>
                            <input hidden type='text' name='messageId' value='" . $msgs['id'] . "'>
                            <input hidden type='text' name='id' value='" . $id . "'>
                        </form>
                    </div>
            </div>
     </div> ";
}
?>
