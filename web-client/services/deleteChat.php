<?php
session_start();
require '../vendor/autoload.php';
$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);
$chatId = $_POST['chatId'];
$_SESSION['err'] = "";
echo $chatId;
try {
    $response = $client->request('delete', 'chats/delete/' . $chatId, [
        'cookies' => unserialize($_SESSION['jid']),
    ]);
    header('Location: ../chatList.php');
} catch (ClientException $e) {
    echo Psr7\Message::toString($e->getRequest());
    echo "<br>";
    echo "<br>";
    echo Psr7\Message::toString($e->getResponse());
    echo "<br>";
    echo "<br>";
    $exc = Psr7\Message::toString($e->getResponse());
    header('Location: ../chatList.php');
}
?>
