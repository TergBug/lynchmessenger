<?php

session_start();
require '../vendor/autoload.php';

use GuzzleHttp\Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);

$message = $_POST['textMessage'];
$chatId = $_POST['chatId'];
$mId = $_POST['mId'];

if (strlen($message) == 0){
    header("Location: ../chatMessage?id=".$mId);
}

$message = str_replace('--sad','(╯︵╰,)',$message);
$message = str_replace('--lynch','( ͡°( ͡° ͜ʖ( ͡° ͜ʖ ͡°)ʖ ͡°) ͡°)',$message);
$message = str_replace('--cute','(◕‿◕✿)',$message);
$message = str_replace('--up','(°ロ°)☝',$message);
$message = str_replace('--npc','˙ ͜ʟ˙',$message);
$message = str_replace('--ftm','（╯°□°）╯︵( .o.)',$message);
$message = str_replace('--tf','(ง°ل͜°)ง',$message);
echo $message;

try{
$response = $client->request('POST', 'messages/add', [
    'json' => [
        'chatId' => $chatId,
        'timeZone' => "Europe/Kiev",
        'text' => $message,
    ],
    'cookies' => unserialize($_SESSION['jid']),
]); }
catch (ServerException $e) {
    echo Psr7\Message::toString($e->getRequest());
    echo "<br>";
    echo "<br>";
    echo Psr7\Message::toString($e->getResponse());
    echo "<br>";
    echo "<br>";
    $exc = Psr7\Message::toString($e->getResponse());
    if ($exc) {
        header('Location: ../chatMessage?id='.$mId);
    } else {
    }   $_SESSION['err'] = "Something went wrong :/"; }

$code = $response->getStatusCode();
$body = $response->getBody();
echo "<br> <br>";
$json = json_decode($body, true);
$_SESSION['chat'] = $json;
header("Location: ../chatMessage?id=".$mId);
