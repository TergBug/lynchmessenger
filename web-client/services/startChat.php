<?php

session_start();
require '../vendor/autoload.php';

use GuzzleHttp\Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);

$find = $_POST['find'];

if ($find == "/clear") {
    $_SESSION['err'] = "";
    header('Location: ../chatList.php');
    return;
}

try {
    $response = $client->request('POST', "chats/start/" . $find, [
        'cookies' => unserialize($_SESSION['jid']),
    ]);
    $_SESSION['err'] = "";
} catch (ClientException $e) {
    echo Psr7\Message::toString($e->getRequest());
    echo "<br>";
    echo "<br>";
    echo Psr7\Message::toString($e->getResponse());
    echo "<br>";
    echo "<br>";
    $exc = Psr7\Message::toString($e->getResponse());

    if ($exc) {
        header('Location: ../chatList.php');
    } else {
    }
    $_SESSION['err'] = "Something went wrong :/";
}

$body = $response->getBody();
$json = json_decode($body, true);
header("Location: ../chatList");
?>
