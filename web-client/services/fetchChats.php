<?php

//session_start();
//require '../vendor/autoload.php';
//require 'services/fetchChats.php';

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);

$response = $client->request('GET', 'chats/fetch', [
    'cookies' => unserialize($_SESSION['jid']),
]);

$code = $response->getStatusCode();
$body = $response->getBody();
$json = json_decode($body, true);
$_SESSION['chat'] = $json;
