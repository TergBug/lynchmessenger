<?php
session_start();
require '../vendor/autoload.php';

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);

$name = $_POST['name'];
    $response = $client->request('PUT', 'users/update', [
    'json' => [
        'name' => $name,
        'avatarLink' => "http"
    ] ,  'cookies' => unserialize($_SESSION['jid'])
]);
$body = $response ->getBody();
$json =  json_decode($body, true);
$_SESSION['user'] = $json;
header("Location: ../chatList");
?>
