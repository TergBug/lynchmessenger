<?php

session_start();
require '../vendor/autoload.php';
error_reporting(0);
ini_set('display_errors', 0);

$_SESSION['base'] = "localhost:8080/";

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);
$nickname = $_POST['username'];
$password = $_POST['password'];

if (unserialize($_SESSION['jid']) != null) {
    header("Location: ../chatList");
    $_SESSION['rerr'] = "";
    return;
} else {
    $_SESSION['rerr'] =
        "<h4 id='signupError'> *Wrong login or password  </h4> ";
    header("Location: ../index");
}
$response = $client->request('POST', 'users/login', [
    'form_params' => [
        'nickname' => $nickname,
        'password' => $password,
    ],
]);

$body = $response->getBody();
$json = json_decode($body, true);
echo $body;

$_SESSION['user'] = $json;
$_SESSION['jid'] = serialize($client->getConfig('cookies'));
$_SESSION['err'] = "";

header("Location: ../chatList");
