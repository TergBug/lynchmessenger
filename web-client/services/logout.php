<?php

session_start();
require '../vendor/autoload.php';

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);
echo "logout <br><br>";
echo $_SESSION['jid'];

if (unserialize($_SESSION['jid']) == null) {
    header("Location: ../login");
    return;
}

$response = $client->request('DELETE', 'users/logout', [
    'cookies' => unserialize($_SESSION['jid']),
]);

$_SESSION['jid'] = null;
$_SESSION['user'] = null;

session_destroy();
header("Location: ../login");
