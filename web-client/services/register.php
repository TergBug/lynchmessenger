<?php

session_start();
require '../vendor/autoload.php';

use GuzzleHttp\Response;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\ClientException;

$client = new GuzzleHttp\Client(['base_uri' => 'localhost:8080/']);

$nickname = $_POST['username'];
$password = $_POST['password'];

try {
    $response = $client->request('POST', 'users/register', [
        'json' => ['nickname' => $nickname, 'password' => $password],
    ]);
    $_SESSION['rerr'] =
        "<h4 id='signupSuccess'> *Registration went successful </h4> ";
    header('Location: ../index.php');
} catch (ClientException $e) {
    $exc = Psr7\Message::toString($e->getResponse());
    if ($exc) {
        $_SESSION['rerr'] = "<h4 id='signupError'> *Wrong login or password  </h4> ";
        header('Location: ../signup.php');
    } else {
        header('Location: ../signup.php');
    }
}
