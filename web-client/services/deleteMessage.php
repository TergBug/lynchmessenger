<?php
session_start();
require '../vendor/autoload.php';

$client = new GuzzleHttp\Client([
    'base_uri' => $_SESSION['base'],
    'cookies' => true,
]);
$id = $_POST['id'];
echo $id;

$_SESSION['err'] = "";
$messageId = $_POST['messageId'];

echo $messageId;
echo "<BR>";
echo $_SERVER['REQUEST_URI'];

try {
    $response = $client->request('delete', 'messages/delete/' . $messageId, [
        'cookies' => unserialize($_SESSION['jid']),
    ]);
   echo $_SERVER['REQUEST_URI'];
    header('Location: ../messages?id='.$id);
   // header('Location: ../chatMessage?id=0');
} catch (ClientException $e) {
    echo Psr7\Message::toString($e->getRequest());
    echo "<br>";
    echo "<br>";
    echo Psr7\Message::toString($e->getResponse());
    echo "<br>";
    echo "<br>";
    $exc = Psr7\Message::toString($e->getResponse());

    if ($exc) {
        header('Location: ../messages?id='.$id);
    } else {    }
}
?>
