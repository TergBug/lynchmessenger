<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> Lynch </title>
    <link rel="stylesheet" href="../css/main.css">
    <link rel="stylesheet" href="../css/chatList.css">
    <link rel="stylesheet" href="../css/chatMessage.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/chatList.css">
    <link rel="stylesheet" href="css/chatMessage.css">
    <script type="text/javascript">
        function hideEdit() {
            var b = document.getElementById("profileEdit");
            b.style.display = "none";
        }
        function hide() {
            var profileEdit = document.getElementById("profileEdit");
            var chatList = document.getElementById("chatList");
            var image = document.getElementById("headerButton");
            var searchBox = document.getElementById("searchBox");
            if (image.getAttribute('src') === 'pics/functional/rotated.png') {
                image.src = "pics/functional/menu.png";
            } else {
                image.src = "pics/functional/rotated.png";
            }
            //chatMessage
            if (profileEdit.style.display == "none") {
                profileEdit.style.display = "block";
                document.getElementById("messagesFrame").style.display = "none";
                document.getElementById("chatWith").style.display = "none";
                document.getElementById("messageInterface").style.display = "none";
            } else {
                location.reload();
            }
            //chatMessage
        }
        document.onkeyup = function(evt) {
            var keyCode = evt ? (evt.which ? evt.which : evt.keyCode) : event.keyCode;
            if (keyCode == 13) {
                //your function call here
                //   document.textForm.submit();
            }
        }
        //specify path to your external page:
        var myIframe = document.getElementById('messagesFrame');

        function sendMessage() {
            var a = document.getElementById("sendButton");
            a.submit;
        }
    </script>

    <style>
        #messagesFrame {
            width: 100%;
            //  height: 500px;
            float: left;
            height: calc(100% - 70px - 61px - 120px);
        }
    </style>
</head>

<?php
session_start();
    require 'vendor/autoload.php';
    require 'services/fetchChats.php';
    $id = $_GET['id'];
    ?>

<body onload="hideEdit()">
    <div class="side-col"></div>
    <div class="mid-col no select">
        <div class="header noselect">
            <div class="menu-button"> <img class="noselect" onclick="hide()" id="headerButton" src="pics/functional/menu.png"> </div>
            <div class="title noselect">LynchMessenger</div>
        </div>
        <div id="chatWith" class="chatMessageWith noselect">
            <a href="chatList.php">
                <div class="chatMessageButton" style="background-image: url(pics/functional/back.PNG)"> </div>
            </a>
            <div class="chatMessagePfp" style="background-image: url(<?php echo $_SESSION['chat'][$id]['userAvatar'] ?>)"> </div>
            <div class="chatMessageUser"> <?php echo $_SESSION['chat'][$id]['userNickname'] ?> </div>
        </div>

        <?php
 $_SESSION['history'] = $_SERVER['REQUEST_URI'];
usort($_SESSION['chat'][$id]['messages'], function ($item1, $item2) {
    return $item1['time'] <=> $item2['time'];});
?>
        <iframe id="messagesFrame" src="messages.php?id=<?php echo $id ?>"></iframe>
        <div id="profileEdit" class="edit">
            <br>
            <br>
            <div class="profilePictureSideFiller">  </div>
            <div class="profilePicture" style="background-image:url(<?php echo $_SESSION['user']['avatarLink'] ?> );"> </div>
            <div class="profilePictureSideFiller">
                <form action="services/deleteAvatar.php" method="post">
                    <input type="text" name="name" value="<?php echo $_SESSION['user']['name'] ?>" hidden>
                    <input type="image" class="pictureFunctions" src="pics/functional/delete.PNG">
                </form>
            </div>
            <form method="post" action="services/updateUser.php">
                <input hidden name="mId" type="text" value="<?php  echo $id ?>">
                <div class="datas">
                    <div class="profileInfo">
                        <div id="profileDataHead" class="profileData noselect"> Nickname</div>
                        <div id="profileDataValue" class="profileData">
                            <input id="profileData" type="text" readonly value="<?php echo $_SESSION['user']['nickname'] ?>">
                        </div>
                        <div id="profileDataHead" class="profileData noselect"> Public Name </div>
                        <div id="profileDataValue" class="profileData">
                            <input name="name" id="profileData" type="text" value="<?php echo $_SESSION['user']['name'] ?>">
                        </div>
                        <div id="profileDataHead" class="profileData noselect"> Profile picture [link] </div>
                        <div id="profileDataValue" class="profileData">
                            <input name="link" id="profileDataLink" type="text" value="<?php echo $_SESSION['user']['avatarLink'] ?>">
                        </div>
                    </div>
                    <br>
                    <br>
                </div>
                <div class="submit">
                    <button id="submitEdit" type="submit">SUBMIT</button>
                </div>
            </form>
            <div class="logout">
                <a href="services/logout.php">
                    <input id="logoutButton" type="submit" value="Logout"> </a>
            </div>
        </div>
        <form name="textForm" class="texterForm" action="services/sendMessage.php" method="post">
            <input hidden name="chatId" type="text" value="<?php  echo $_SESSION['chat'][$id]['id'] ?>">
            <input hidden name="mId" type="text" value="<?php  echo $id ?>">
            <div id="messageInterface" class="messageInterface">
                <div class="singleMessageTexterSideCol">
                    <div class="singleMessageTexterPfp noselect" style="background-image: url(<?php  echo $_SESSION['user']['avatarLink'] ?>)"></div>
                </div>

                <div class="singleMessageTexterMidCol">
                    <textarea name="textMessage" class="texterInput" placeholder="Write a message.."></textarea>
                </div>
                <div class="singleMessageTexterSideCol noselect">
                    <input id="sendButton" type="image" class="singleMessageTexterButton" onclick="sendMessage()" src="pics/functional/send.png">
                </div>
            </div>
        </form>
    </div>
    <div class="side-col"></div>
</body>
</html>
